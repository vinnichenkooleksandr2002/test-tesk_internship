import pandas as pd
import numpy as np

from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error, precision_score
from sklearn.model_selection import train_test_split

data = pd.read_csv('internship_train.csv')
data = np.array(data)
X, y = data[:, 6].reshape((-1, 1)), data[:, -1].reshape((-1, 1))

X_prep = X**2

X_train, X_test, y_train, y_test = train_test_split(X_prep, y, random_state=42)
model = LinearRegression()
model.fit(X_train, y_train)

plt.scatter(X_prep, y, color='red')
plt.scatter(X_prep, model.predict(X_prep), color='green')
plt.show()
# print(mean_squared_error(y_test, model.predict(X_test)))
# print(model.coef_)
# print(np.mean(y))
data2 = pd.read_csv('internship_hidden_test.csv')
data2 = np.array(data2)

X = data2[:, 6].reshape((-1, 1))
# print(model.predict(X**2))
print(model.predict(X**2))
res = np.append(data2, model.predict(X**2), axis=1)
res = pd.DataFrame(res)
res.to_csv('model_predictions.csv', mode='a', index=False, header=True)

