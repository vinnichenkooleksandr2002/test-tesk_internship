def func(num: int) -> int | float:
    return num * (num + 1) // 2


print(func(34))
