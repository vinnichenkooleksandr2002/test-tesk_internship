"""Function realization"""
from dataclasses import dataclass
from enum import Enum
from pprint import pprint

# def neighbours(_x, _y, n, m):
#     nearly = []
#     if _x != 0:
#         nearly.append((_x - 1, _y))
#     if _x != m - 1:
#         nearly.append((_x + 1, _y))
#     if _y != 0:
#         nearly.append((_x, _y - 1))
#     if _y != n - 1:
#         nearly.append((_x, _y + 1))
#     return nearly
#
#
# matrix = [
#     [0, 1, 0],
#     [0, 0, 1],
#     [1, 0, 1]
# ]
# N = len(matrix[0])
# M = len(matrix)
# visited = [[0 for _ in range(N)] for _ in range(M)]
#
# stack = []
# count = 0
# for i in range(M):
#     for j in range(N):
#         if matrix[i][j] == 1 and visited[i][j] == 0:
#             stack.append((i, j))
#             while stack:
#                 x, y = stack.pop()
#                 visited[x][y] = 1
#                 for x_, y_ in neighbours(x, y, N, M):
#                     if visited[x_][y_] == 0 and matrix[x_][y_] == 1:
#                         stack.append((x_, y_))
#             count += 1
# print(count)
#
"""Class realization"""


class IslandStatus(Enum):
    ISLAND = 1
    WATER = 0

@dataclass
class Cell:
    x: int
    y: int
    value: IslandStatus
    visited: bool = False





@dataclass
class Map:
    height: int
    width: int
    cells: list[list[Cell]]

    @classmethod
    def from_number_metrix(cls, matrix: list[list[int]]):
        cells = [[Cell(x, y, IslandStatus(value)) for x, value in enumerate(row)] for y, row in enumerate(matrix)]
        return cls(len(matrix), len(matrix[0]), cells)

    @classmethod
    def from_string(cls, string: str):
        matrix = [[1 if value == '#' else 0 for value in row] for row in string.split('\n')]
        return cls.from_number_metrix(matrix)


    def neighbours(self, cell: Cell) -> list[Cell]:
        nearly = []
        if cell.x != 0:
            nearly.append(self.cells[cell.y][cell.x - 1])
        if cell.x != self.width - 1:
            nearly.append(self.cells[cell.y][cell.x + 1])
        if cell.y != 0:
            nearly.append(self.cells[cell.y - 1][cell.x])
        if cell.y != self.height - 1:
            nearly.append(self.cells[cell.y + 1][cell.x])
        return nearly


    def get_island_count(self):
        stack = []
        count = 0
        for i in range(self.height):
            for j in range(self.width):
                if self.cells[i][j].value == IslandStatus.ISLAND and not self.cells[i][j].visited:
                    stack.append(self.cells[i][j])
                    while stack:
                        cell = stack.pop()
                        cell.visited = True
                        for cell_ in self.neighbours(cell):
                            if not cell_.visited and cell_.value == IslandStatus.ISLAND:
                                stack.append(cell_)
                    count += 1
        return count

    @property
    def island_count(self):
        return self.get_island_count()

# matrix = [
#     [0, 1, 0],
#     [0, 0, 1],
#     [1, 0, 1]
# ]
# map_ = Map.from_number_metrix(matrix)
# print(map_.return_island_count())

with open('map.txt', 'r') as file:
    map_ = Map.from_string(file.read().strip())
print(map_.island_count)
